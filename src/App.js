import { ThemeProvider } from 'styled-components';
import Navigation from './components/Navigation';

//Style
import GlobalStyle from './styles/GlobalStyles';

//Theme
import { light } from './styles/Themes'

function App() {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={light}>
        <Navigation />
      </ThemeProvider>
    </>
  );
}

export default App;
